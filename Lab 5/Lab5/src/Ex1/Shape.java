/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex1;

abstract class Shape {
    public String colour;
    public boolean filled;

    public Shape()
    {
        colour="green";
        filled=true;
    }
    public Shape(String colour,boolean filled)
    {
        this.colour=colour;
        this.filled=filled;
    }
    public String getColour()
    {
        return colour;
    }
    public void  setColour(String colour)
    {
        this.colour=colour;
    }
    public boolean isFilled()
    {
        return filled;
    }
    public void setFilled(boolean filled)
    {
        this.filled=filled;
    }
   abstract  double getArea();
   abstract  double getPerimeter();
 @Override
   public abstract   String toString();
  
 
}