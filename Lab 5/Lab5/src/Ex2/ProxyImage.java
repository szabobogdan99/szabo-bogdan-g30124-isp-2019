/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex2;

/**
 *
 * @author Sala 310
 */
public class ProxyImage implements Image{
 
   private RealImage realImage;
   private String fileName;
   private RotatedImage rotatedimageName;
   private boolean c;
 
   public ProxyImage(String fileName,boolean c){
      this.fileName = fileName;
      this.c=c;
   }
  
 
   @Override
   public void display() {
      if(realImage == null){
         realImage = new RealImage(fileName);
      }
      if(rotatedimageName == null){
         rotatedimageName = new RotatedImage(fileName);
      }
      if(c==true)
      {
      realImage.display();
      }
      else
      {
          rotatedimageName.display();
      }
   }
}