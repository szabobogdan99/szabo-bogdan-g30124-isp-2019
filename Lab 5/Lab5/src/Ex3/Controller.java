/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex3;

/**
 *
 * @author Sala 310
 */
public class Controller {
    TemperatureSensor t ;
    LightSensor l;
    public Controller (TemperatureSensor t,LightSensor l)
    {
        this.t=t;
        this.l=l;
    }
    public void control()
    {
        int running=0;
        while(running<20)
        {
            try {
                System.out.println("The temperature is: "+t.readValue()+" and the light is: "+l.readValue());
                Thread.sleep(1000);
                running++;
            }catch(Exception e){

            }
        }
    }
   
    public String toString()
    {
        return "The temperature is: "+t.readValue()+" and the light is: "+l.readValue();
    }
}
