package Ex4;

public class Controller {
    TemperatureSensor t ;
    LightSensor l;
    private static Controller controller;
    private Controller (TemperatureSensor t,LightSensor l)
    {
        this.t=t;
        this.l=l;
    }

    public static Controller getController()
    {
        if (controller==null)
        {
            TemperatureSensor s1=new TemperatureSensor();
            LightSensor s2=new LightSensor();
            controller=new Controller(s1,s2);

        }
        return controller;
    }
    public void control()
    {
        int running=0;
        while(running<20)
        {
            try {
                System.out.println("The temperature is: "+t.readValue()+" and the light is: "+l.readValue());
                Thread.sleep(1000);
                running++;
            }catch(Exception e){

            }
        }
    }


    public String toString()
    {
        return "The temperature is: "+t.readValue()+" and the light is: "+l.readValue();
    }
}
