package Ex4;

abstract class Senzor {
    String location;
    abstract int readValue();
    public String getLocation()
    {
        return this.location;
    }


}
