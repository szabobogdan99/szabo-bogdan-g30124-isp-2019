package Ex4;

import java.util.Random;
/**
 *
 * @author Sala 310
 */
public class TemperatureSensor extends Senzor {

    @Override
    int readValue() {
        Random rand = new Random();
        int n = rand.nextInt(100);
        return n;
    }


}
