package Ex3;

public class Counters extends Thread {
    Thread t;
    String name;
    Counters (String name,Thread t)
    {
        this.t=t;
        this.name=name;

    }
    public void run()

    {
        int x=0;
        try
        {
            if (t!=null) t.join();

            for(int i=1;i<=2;i++){
                x++;
                System.out.println(getName() + " x = "+x);
                try {

                    Thread.sleep((int)(Math.random() * 1000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(getName() + " job finalised.");
        }
        catch(Exception e){e.printStackTrace();}
    }

    public static void main(String[] args)
    {
        Counters w1 = new Counters("Proces 1",null);
        Counters w2 = new Counters("Proces 2",w1);
        w1.start();
        w2.start();
    }
}
