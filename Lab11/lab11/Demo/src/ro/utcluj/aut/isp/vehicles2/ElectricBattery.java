package ro.utcluj.aut.isp.vehicles2;
import java.io.*;
public class ElectricBattery {

    /**
     * Percentage load.
     */
    private int charge = 0;

    public void charge() throws BatteryException{
        
         if(charge>=100)
        {
            throw new BatteryException("Full battery");
        }
         else
         {
             charge++;
         }
            
       
        
    }

}
