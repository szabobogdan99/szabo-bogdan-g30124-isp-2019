package ro.utcluj.aut.isp.vehicles2;

public class Vehicle {

    private String type;
    private int weight;

    public Vehicle(String type, int length) {
        this.type = type;
        weight=length;
    }

    public String start(){
        return "engine started";
    }
 public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicle vehicle = (Vehicle) o;
        return weight == vehicle.weight &&
                type.equals(vehicle.type);
    }
}
