/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex1;

import java.util.Observable;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sala 310
 */
public class Senzor extends Observable implements Runnable {
        private boolean active=true;
        private int temp;


    public void run(){
        Random rand=new Random();
        while(active){
            temp=rand.nextInt(50);
            rand=new Random();
            setChanged();
            notifyObservers(new Integer(temp));
            
            
            
            
           try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Senzor.class.getName()).log(Level.SEVERE, null, ex);
            }

                  }
            }

    public int getTemp() {
        return temp;
    }
    
    

}
    

