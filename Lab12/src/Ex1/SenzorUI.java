/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex1;

import java.awt.FlowLayout;
 
import javax.swing.*;
import java.util.*;
 
public class SenzorUI extends JFrame implements Observer{
 
      HashMap accounts = new HashMap();
 
      JLabel t;
      JTextField tTemp;
     
 
   SenzorUI(){
 
           
            setTitle("Termometru");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            init();
            setSize(200,250);
            setVisible(true);
            
            Senzor k = new Senzor();
            k.addObserver(this);
            Thread t= new Thread(k);
            t.start();
                   
      }
 
      public void init(){
 
            this.setLayout(null);
            int width=80;int height = 20;
 
            t = new JLabel("Temp ");
            t.setBounds(10, 50, width, height);
             
        
 
            tTemp = new JTextField();
            tTemp.setBounds(70,50,width, height);
            tTemp.setEditable(false);
 
 
         
 
            add(t);add(tTemp);
 
      }
 
      public static void main(String[] args) {
            new SenzorUI();
      }

    @Override
    public void update(Observable o, Object arg) {
    tTemp.setText(arg.toString());
    }
}