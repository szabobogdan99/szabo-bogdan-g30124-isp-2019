package Ex2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Bank {

    ArrayList<BankAccount> b = new ArrayList<>();

    void addAccount(String owner, double balance) {
        BankAccount bankAccount = new BankAccount(owner, balance);
        b.add(bankAccount);

    }

    class Comp implements Comparator<BankAccount> {
        @Override
        public int compare(BankAccount e1, BankAccount e2) {
            return Double.compare(e1.getBalance(), e2.getBalance());
        }
    }

    void printAccounts() {
        Collections.sort(b, new Comp());
        for (BankAccount str : b) {
            System.out.println(str.getOwner() + "  " + str.getBalance());
        }
    }

    void printAccounts(double minBalance, double maxBalance) {
        Collections.sort(b, new Comp());
        for (BankAccount bankAccount : b)
            if (minBalance < bankAccount.getBalance() && maxBalance > bankAccount.getBalance())
                System.out.println(bankAccount.getOwner() + " " + bankAccount.getBalance());
            else System.out.println("Nu este intre min si max");
    }

//    public BankAccount getAllAccount(String owner) {
//        for (BankAccount str : b) {
//            if (str.getOwner().equals(owner))
//                return str;
//        }
//        return null;
//    }

    public ArrayList<BankAccount> getAllAccount() {
        return b;
    }


    public static void main(String[] args) {

        Bank b = new Bank();

        b.addAccount("nume1", 1000);
        b.addAccount("nume2", 3000);
        b.addAccount("nume3", 3500);
        System.out.println("Lista sortata dupa balance");
        b.printAccounts();

        System.out.println();
        System.out.println("Afisarea conturilor cuprinse intre doua val:");
        b.printAccounts(200, 3000);

        ArrayList<BankAccount> bankAccounts = b.getAllAccount();

        Collections.sort(bankAccounts,new BankAccount.ownerComparator());

        System.out.println();
        System.out.println("Toate conturile in mod alfabetic:");
        for(BankAccount b1:bankAccounts)
            System.out.println(b1.getBalance()+" "+b1.getOwner());
    }
}
