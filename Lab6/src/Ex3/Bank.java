package Ex3;

import java.util.Comparator;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Bank {

    TreeSet<BankAccount> b = new TreeSet<>();

    void addAccount(String owner, double balance) {

        b.add(new BankAccount(owner,balance));
    }

    class Comp implements Comparator<BankAccount> {
        @Override
        public int compare(BankAccount e1, BankAccount e2) {
            return Double.compare(e1.getBalance(), e2.getBalance());
        }
    }

    void printAccounts() {
        Comparator<BankAccount> byBalance=Comparator.comparingDouble(BankAccount::getBalance);
        Supplier<TreeSet<BankAccount>> suplier=()->new TreeSet<BankAccount>(byBalance);

        TreeSet<BankAccount> sort = new TreeSet<>();
        sort=b.stream().collect(Collectors.toCollection(suplier));
        for(BankAccount b: sort)
            System.out.println(b.getBalance()+" "+b.getOwner());

    }

    void printAccounts(double minBalance, double maxBalance) {
        for(BankAccount bankAccount:b)
            if(bankAccount.getBalance()>minBalance && bankAccount.getBalance()<maxBalance)
                System.out.println(bankAccount.getBalance()+" "+bankAccount.getOwner());
    }

    public TreeSet<BankAccount> getAllAcount(){
        Comparator<BankAccount> byName=Comparator.comparing(BankAccount::getOwner);
        Supplier<TreeSet<BankAccount>> suplier=()->new TreeSet<BankAccount>(byName);

        return b.stream().collect(Collectors.toCollection(suplier));

    }


    public static void main(String[] args) {

        Bank b = new Bank();

        b.addAccount("nume1", 1000);
        b.addAccount("nume2", 3000);
        b.addAccount("nume3", 3500);
        System.out.println("Lista sortata dupa balance:");
        b.printAccounts();

        System.out.println();
        System.out.println("Afisarea conturilor cuprinse intre doua val:");
        b.printAccounts(200, 3000);

        System.out.println();
        System.out.println("Toate conturile in mod alfabetic:");
        for(BankAccount b1:b.getAllAcount())
            System.out.println(b1.getOwner()+" "+b1.getBalance());
    }
}
