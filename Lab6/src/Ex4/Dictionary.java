package Ex4;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.Scanner;

public class Dictionary {
    HashMap<Word,Definition> hashMap = new HashMap<Word,Definition>();

    public void addWord(Word w, Definition d)
    {
        hashMap.put(w,d);
    }
    public Definition getDefinition(Word w)
    {
        return hashMap.get(w);
    }
    public Set<Word> getAllWords()
    {
        return hashMap.keySet();
    }
    public Collection<Definition> getAllDefinitions()
    {
        return hashMap.values();
    }

    public HashMap<Word, Definition> getHashMap() {
        return this.hashMap;
    }

    public static void main(String[] args) {
        Dictionary d = new Dictionary();
        Word w1 = new Word("Cablu");
        Word w2 = new Word("Laptop");
        Definition d2 = new Definition("Calculator");
        Definition d1 = new Definition("Fir");
        d.addWord(w1, d1);
        d.addWord(w2, d2);
        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();
        while (true) {

            switch (choice) {

                case 1:
                    System.out.println(d.getDefinition(w1).getDefinition());
                    System.out.println();
                    break;
                case 2:
                    Set<Word> words = d.getAllWords();
                    for (Word w : words)
                        System.out.println(w.getName());
                    System.out.println();
                    break;
                case 3:
                    Collection<Definition> definitions = d.getAllDefinitions();
                    for (Definition def : definitions)
                        System.out.println(def.getDefinition());
                    break;
                case 4:
                    return;

                default:
                    System.out.println("Wrong choice");
            }
            scanner = new Scanner(System.in);
            choice = scanner.nextInt();
        }
    }

}

