package Ex2;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import javax.swing.*;
import java.util.*;
public class Counter extends JFrame {

    static int nr=0;
    JLabel label;
    JTextArea val;
    JButton inc;
    Counter()
    {


        setTitle("Incrementare");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,250);
        setVisible(true);
        val.setEditable(false);

    }
    public void init()
    {
        this.setLayout(null);
        int width=80;int height = 20;
        label = new JLabel("Incrementare ");
        label.setBounds(10, 50, width, height);
        val = new JTextArea();
        val.setBounds(100,75,width, height);
        inc = new JButton("Incrementare");
        inc.setBounds(10,150,width, height);

        inc.addActionListener(new TratareInc());
        add(label);add(val);add(inc);


    }
    public static void main(String[] args) {
        new Counter();
    }

    class TratareInc implements ActionListener
    {
        public void actionPerformed(ActionEvent e){


            nr++;
            Counter.this.val.setText(String.valueOf(nr));


        }
    }

}
