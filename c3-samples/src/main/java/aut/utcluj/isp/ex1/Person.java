package aut.utcluj.isp.ex1;

/**
 * @author stefan
 */
public class Person {
    private String firstName;
    private String lastName;

     public Person() {}
    public Person(String firstName) {
        this.lastName = "";
         this.firstName = firstName;
    }

    public Person(String firstName, String lastName) {
        this.lastName = lastName;
         this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

     public void setFirst(String firstName) {
        firstName=firstName;
    }
     public void setLast(String lastName) {
        lastName=lastName;
    }
    public String getLastName() {
        return lastName;
    }
    
   @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person m = (Person) o;
        return firstName.equals(m.firstName) && lastName.equals(m.lastName);
    }
    
    @Override
    public String toString()
    {
        return this.firstName+" "+this.lastName;
    }
}