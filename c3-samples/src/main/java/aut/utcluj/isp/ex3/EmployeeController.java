package aut.utcluj.isp.ex3;

import java.util.ArrayList;
import java.util.List;

/**
 * @author stefan
 */
public class EmployeeController {
    /**
     * Add new employee to the list of employees
     *
     * @param employee - employee information
     */
   List<Employee> a=new ArrayList();
    public void addEmployee(final Employee employee) {
        a.add(employee);
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Get employee by cnp
     *
     * @param cnp - unique cnp
     * @return found employee or null if not found
     */
    public Employee getEmployeeByCnp(final String cnp) {
        for (int i = 0; i < a.size(); i++) {
            if(a.get(i).getCnp()==cnp){
                return a.get(i);
                }
        }
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Update employee salary by cnp
     *
     * @param cnp    - unique cnp
     * @param salary - salary
     * @return updated employee
     */
    public Employee updateEmployeeSalaryByCnp(final String cnp, final Double salary) {
        for (int i = 0; i < a.size(); i++) {
             if(a.get(i).getCnp()==cnp)
             {
                 a.get(i).salary=salary;
             }
         }
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Delete employee by cnp
     *
     * @param cnp - unique cnp
     * @return deleted employee or null if not found
     */
    public Employee deleteEmployeeByCnp(final String cnp) {
         for (int i = 0; i < a.size(); i++) {
             if(a.get(i).getCnp()==cnp)
             {
                 a.remove(i);
             }
         }
        throw new UnsupportedOperationException("Not supported yet.");
        
    }
         
    

    /**
     * Return current list of employees
     *
     * @return current list of employees
     */
    public List<Employee> getEmployees() {
        return a;
         
    
       // throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Get number of employees
     *
     * @return - number of registered employees
     */
    public int getNumberOfEmployees() {
      
      return a.size();
        //throw new UnsupportedOperationException("Not supported yet.");
         
    }
}

