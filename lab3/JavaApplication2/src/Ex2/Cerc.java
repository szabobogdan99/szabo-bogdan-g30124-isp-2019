/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex2;

/**
 *
 * @author Sala 310
 */
public class Cerc {
    double radius;
    String colour;
    public Cerc()
    {
        this.radius=1.0;
        this.colour="red";
    }
    public Cerc(double radius, String colour)
    {
        this.radius=radius;
        this.colour=colour;
    }
    public double getRadius()
    {
        return this.radius;
    }
    public String getColour()
    {
        return this.colour;
    }
}
