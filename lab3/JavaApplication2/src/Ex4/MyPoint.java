/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex4;

/**
 *
 * @author Sala 310
 */
public class MyPoint {
    int x,y;
    public MyPoint()
    {
        x=0;
        y=0;
    }
    public MyPoint(int x,int y)
    {
        this.x=x;
        this.y=y;
    }
    public int Getx()
    {
        return this.x;
        
    }
      public int Gety()
    {
        return this.y;
        
    }
        public void Setx(int x)
    {
        this.x=x;
    }
         public void Sety(int y)
    {
        this.y=y;
    }
        public void Setxy(int x,int y)
    {
        this.x=x;
        this.y=y;
    } 
        @Override
    public String toString() {
        return "( " + x + " , " +y+" )" ;
                
    }
     public double distance(int x, int y)
     {
         int z;
         return Math.sqrt((x-this.x)*(x-this.x)+(y-this.y)*(y-this.y));
     }
    
    
    
}
