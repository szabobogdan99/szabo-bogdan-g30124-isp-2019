package ex5;

public class Circle {
    public double radius;
    private String colour;
    public Circle()
    {
        radius=1.0;
        colour="red";
    }
    public Circle(double radius)
    {
        this.radius=radius;

    }
    public double getArea(){return 2*3.14*radius;}
    public double getRadius()
    {
        return radius;
    }
    public String getColour()
    {
        return colour;
    }
}