package ex5;

public class TestCylinder {
    public static void main(String[] args) {
        Cylinder s1=new Cylinder();
        System.out.println("Inaltimea: "+s1.getHeight());
        s1=new Cylinder(4);
        System.out.println("Raza: "+s1.getRadius());
        double x1=s1.getArea();
        double x2=s1.getVolume();
        System.out.println(("Aria: "+x1));
        System.out.println(("Volumul: "+x2));

    }
}