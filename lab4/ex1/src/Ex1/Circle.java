package Ex1;

public class Circle {
    private double radius;
    private String colour;
    public Circle()
    {
        radius=1.0;
        colour="red";
    }
    public Circle(double radius)
    {
        this.radius=radius;

    }
    public double getRadius()
    {
        return radius;
    }
    public String getColour()
    {
        return colour;
    }
}


