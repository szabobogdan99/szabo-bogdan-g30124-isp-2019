package Ex6;

public class Circle extends Shape {
    public double radius;

    public Circle()
    {
        radius=1.0;

    }
    public Circle(double radius)
    {
        this.radius=radius;

    }
    public Circle(double radius, String colour,boolean filled)
    {
        super(colour,filled);
        this.radius=radius;
    }
    public double getRadius()
    {
        return radius;
    }
    public double getArea(){return 3.14*radius*radius;}
    public double getPerimeter(){return 2*3.14*radius; }
   @Override
    public String toString()
   {
       return "A Circle with radius= "+radius+" ,which is a subclass of: "+super.toString();
   }


}