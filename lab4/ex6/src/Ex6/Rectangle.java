package Ex6;

public class Rectangle extends Shape{
    public double width;
    public double length;
    public Rectangle()
    {
        width=1;
        length=1;
    }
    public Rectangle(double width,double length)
    {
        this.width=width;
        this.length=length;

    }
    public Rectangle(double width,double length,String colour,boolean filled)
    {
        super(colour,filled);
        this.width=width;
        this.length=length;
    }

    public double getWidth()
    {
        return width;
    }
    public void  setWidth(double width)
    {
        this.width=width;
    }
    public double getLength()
    {
        return length;
    }
    public void setLength()
    {
        this.length=length;
    }
    public double getArea()
    {
        return width*length;
    }
    public double getPerimeter()
    {
        return 2*width+2*length;
    }
    public String toString()
    {
        return "A rectangle with width= "+width+" and length "+length+" ,which is a subclass of "+ super.toString();
    }
}
