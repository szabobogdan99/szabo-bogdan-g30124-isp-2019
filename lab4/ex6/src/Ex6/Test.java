package Ex6;

public class Test {
    public static void main(String[] args) {
        Shape s1=new Shape();
        System.out.println(s1);
         s1=new Circle();
        System.out.println(s1);
         System.out.println("Aria: "+((Circle) s1).getArea());
        System.out.println("Perimetru: "+((Circle) s1).getPerimeter());
        s1=new Rectangle();
        System.out.println(s1);
        System.out.println("Aria: "+((Rectangle) s1).getArea());
        System.out.println("Perimetru: "+((Rectangle) s1).getPerimeter());
        s1=new Square(2,"red",false);
        System.out.println(s1);


    }
}
